#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <fstream>
#include<math.h>

using namespace std;

void quickSort(int* array,int left, int right);
int* createArray(int length);
void resetArray(int* originalArray,int* copyArray, int length);
int partition(int* array, int left, int right);
void swap(int& a, int& b);
void merge(int* a, int left, int mid, int right,int size);
void mergeSort (int* a, int left, int right,int size);
void insertionSort (int a[], int left, int right);
void quickInsertionSort(int* a,int left, int right);
void quickMergeSort(int arr[], int size);
void mergeForQuickMerge(int* a, int left, int mid, int right,int size);
void quickSortForQuickMerge(int* a,int left, int right,int size);

int main (int argc, char* argv[])
	{
    int* originalArray;
    int* copyArray;
    ofstream quickSortFile;
    ofstream mergeSortFile;
    ofstream quickInsertFile;
    ofstream quickMergeFile;
    clock_t startTime;
    clock_t endTime;
    double quickSortTime;
    double mergeSortTime;
    double quickInsertTime;
    double quickMergeTime;
    srand(time(NULL));
    quickSortFile.open("quickSort.txt");
    mergeSortFile.open("mergeSort.txt");
    quickInsertFile.open("quickInsert.txt");
    quickMergeFile.open("quickMerge.txt");
    for(int n = 100000; n <= 1000000; n = n + 100000)
            {
            originalArray = createArray(n);
            copyArray = new int[n];

            resetArray(originalArray,copyArray,n);
            startTime = clock();
            quickSort(copyArray,0,n-1);
            quickSortTime = (clock() - startTime)/(double)CLOCKS_PER_SEC;

            resetArray(originalArray,copyArray,n);
            startTime = clock();
            mergeSort(copyArray,0,n - 1,n);
            mergeSortTime = (clock() - startTime)/(double)CLOCKS_PER_SEC;

            resetArray(originalArray,copyArray,n);
            startTime = clock();
            quickInsertionSort(copyArray,0, n);
            quickInsertTime = (clock() - startTime)/(double)CLOCKS_PER_SEC;

            resetArray(originalArray,copyArray,n);
            startTime = clock();
			quickMergeSort(copyArray,n);
            quickMergeTime = (clock() - startTime)/(double)CLOCKS_PER_SEC;

            cout << n << ")" << endl;
            cout << "QS: " << fixed << setw(5) << setprecision(2) << quickSortTime << " sec" << endl;
            cout << "MS: " << fixed << setw(5) << setprecision(2) << mergeSortTime << " sec" << endl;
            cout << "QI: " << fixed << setw(5) << setprecision(2) << quickInsertTime << " sec" << endl;
            cout << "QM: " << fixed << setw(5) << setprecision(2) << quickMergeTime << " sec" << endl;
            cout << endl;
            quickSortFile << n << "\t" << quickSortTime << endl;
            mergeSortFile << n << "\t" << mergeSortTime << endl;
            quickInsertFile << n << "\t" << quickInsertTime << endl;
            quickMergeFile << n << "\t" << quickMergeTime << endl;
            delete[] originalArray;
            delete[] copyArray;
            }
        cout << "done" << endl;
    quickSortFile.close();
    mergeSortFile.close();
    quickInsertFile.close();
    quickMergeFile.close();
	}
//===========================================================
//Constructs and randomly fills array
int* createArray(int length)
    {
    int randomNum;
    int* originalArray = new int[length];
    for(int n = 0; n < length; n++)
        {
        randomNum = rand() % 100 + 1;
        originalArray[n] = randomNum;
        }
    return originalArray;
    }
//===========================================================
//Sets array back to the original array constructed from createArray
void resetArray(int* originalArray,int* copyArray, int length)
    {
    for(int n = 0; n < length; n++)
        copyArray[n] = originalArray[n];
    }
//===========================================================
//Implementation of Quick Sort Algorithm
void quickSort(int* a,int left, int right)
	{
    if (left < right)
        {
        if (a[left] > a[right])
            swap (a[left], a[right]);
        int k = partition (a, left, right);
        quickSort (a, left, k-1);
        quickSort (a, k+1, right);
        }
	}
//===========================================================
//Partitions array for Quick Sort Algorithm
int partition(int* a,int left, int right)
	{
    int i = left;
    int j = right + 1;
    int pivot = a[left];
    do
        {
        do
            i++;
        while (a[i] < pivot);
        do
            j--;
        while (a[j] > pivot);
        if(i<j)
            swap (a[i], a[j]);
        }
    while (i < j);
    swap (a[j], a[left]);
    return j;
	}
//=========================================================
//Swaps the value of 2 elements for Quick Sort Algorithm
void swap(int& a, int& b)
    {
    int temp;
    temp = a;
    a = b;
    b = temp;
    }
//===========================================================
//Implementation of Merge Sort Algorithm
void mergeSort (int* a, int left, int right,int size)
{
if (left < right)
    {
    int mid = (left + right) / 2;
    mergeSort (a, left, mid,size);
    mergeSort (a, mid+1, right,size);
    merge (a, left, mid, right,size);
    }
}
//===========================================================
//Merge the 2 halves created from Merge Sort Algorithm
void merge(int* a, int left, int mid, int right,int size)
    {
    int i = left;
    int j = mid + 1;
    int k = left;
    int* b;
    b = new int[size];
    while (i <= mid && j <= right)
        {
        if (a[i] <= a[j])
            b[k++] = a[i++];
        else
            b[k++] = a[j++];
        }
    while (i <= mid)
        b[k++] = a[i++];
    for (i = left; i < j; i++)
        a[i] = b[i];
    delete[] b;
    }
//===========================================================
//Implementation of Quick Insertion Sort Algorithm
void quickInsertionSort(int* a,int left, int right)
	{
    if (left < right)
        {
		if(right - left >= 10)
			{
        	if (a[left] > a[right])
            	swap (a[left], a[right]);
        	int k = partition (a, left, right);
        	quickInsertionSort (a, left, k-1);
        	quickInsertionSort (a, k+1, right);
        	}
        else
        	insertionSort(a,left,right+1);
		}
	}
//===============================================================
//Used in Quick Insertion Sort Algorithm when array size is less than 10
void insertionSort (int a[], int left, int right)
	{
	int v;
	int j;
	for (int i = left + 1; i < right; i++)
		{
		if (a[i] < a[i-1])
			{
			v = a[i];
			j = i;
			while ((j > 0) && (a[j-1] > v))
				{
				a[j] = a[j-1];
				--j;
				}
			a[j] = v;
			}
		}
	}
//===========================================================
//Quick sort implementation for Quick Merge Algorithm
void quickSortForQuickMerge(int* a,int left, int right,int size)
	{
	if(right > size)
		right = size - 1;
    if (left < right)
        {
        if (a[left] > a[right])
            swap (a[left], a[right]);
        int k = partition (a, left, right);
        quickSortForQuickMerge (a, left, k-1,size);
        quickSortForQuickMerge(a, k+1, right,size);
        }
	}
//===============================================================
//Merge sort implementation for Quick Merge Algorithm
void mergeForQuickMerge(int* a, int left, int mid, int right,int size)
    {
    int i = left;
    int j = mid + 1;
    int k = left;
    int* b;
    b = new int[size];
    if(right >= size)
    	right = size - 1;
    if(mid < size)
		{
		while (i <= mid && j <= right)
			{
			if (a[i] <= a[j])
				b[k++] = a[i++];
			else
				b[k++] = a[j++];
			}
		while (i <= mid)
			b[k++] = a[i++];
		for (i = left; i < j; i++)
			a[i] = b[i];
		}
    delete[] b;
    }
//====================================================================================
//Implementation of Quick Merge Algorithm
void quickMergeSort(int* a,int size)
	{
	int n;
	int left;
	int right;
	int mid;
	n = ceil(sqrt(size));
	for(int m = 0; m < size; m = m + n)
		quickSortForQuickMerge(a, m, m + n,size);
	left = 0;
	mid = n - 1;
	right = n * 2;
	for(int m = right; m < size; m = m * 2)
		{
		mergeForQuickMerge(a, left, mid, right,size);
		left = left + m;
		mid = mid + m;
		right = right + m;
		}
	}
