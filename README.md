# Sorting_Algorithms

[Sorting_Algorithms](https://gitlab.com/bamathis/Sorting_Algorithms) is an implemention of several sorting algorithms to test their timing.

## Quick Start

### Program Execution

```
$ ./sort.cpp 
```

### File Output

Writes the times for each algorithm to the files **quickSort.txt**, **mergeSort.txt**, **quickInsert.txt**, and **quickMerge.txt**.
